#include "YeeBulb.h"

#include <getopt.h>
#include <iostream>

bool getHost(int argc, char** argv, std::string& host)
{
    bool ret = true;
    int32_t opt = -1;

    if (argc > 1)
    {
        do
        {
            opt = getopt(argc, argv, "h:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'h':
                        host.assign(optarg);
                        break;
                    default:
                        std::cout << "Usage:   " << argv[0] << " [-option] [argument]" << std::endl;
                        std::cout << "option:  " << std::endl;
                        std::cout << "         " << "-h  Ethernet host (127.0.0.1)" << std::endl;
                        ret = false;
                        break;
                }
            }
        }while (ret && (opt != -1));
    }

    return ret;
}

int main(int argc, char** argv)
{
    int ret = 0;

    std::string host = "127.0.0.1";
    if (getHost(argc, argv, host))
    {
        OSEF::YeeBulb bulb(host);
        bool power = false;
        if (bulb.getPower(power))
        {
            std::cout << "Yeelight bulb " << host << " power " << (power ? "ON" : "OFF") << std::endl;
        }
        else
        {
            ret = -1;
            std::cerr << "error getting power from bulb at " << host << std::endl;
        }

        uint32_t bright = 0;
        if (bulb.getBright(bright))
        {
            ret = 0;
            std::cout << "Yeelight bulb " << host << " bright " << bright << std::endl;
        }
        else
        {
            ret = -1;
            std::cerr << "error getting bright from bulb at " << host << std::endl;
        }
    }

    return ret;
}
