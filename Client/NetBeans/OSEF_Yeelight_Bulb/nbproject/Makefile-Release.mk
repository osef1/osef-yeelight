#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/29dd86f/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra -Werror
CXXFLAGS=-Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../../NetBeans/OSEF_Yeelight/dist/Release/GNU-Linux/libosef_yeelight.a ../../../osef-ssdp/osef-http/osef-netbuffer/osef-posix/NetBeans/OSEF_POSIX/dist/Release/GNU-Linux/libosef_posix.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_yeelight_bulb

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_yeelight_bulb: ../../../NetBeans/OSEF_Yeelight/dist/Release/GNU-Linux/libosef_yeelight.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_yeelight_bulb: ../../../osef-ssdp/osef-http/osef-netbuffer/osef-posix/NetBeans/OSEF_POSIX/dist/Release/GNU-Linux/libosef_posix.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_yeelight_bulb: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/osef_yeelight_bulb ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/29dd86f/main.o: ../../main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/29dd86f
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../../../Yeelight -I../../../osef-ssdp/osef-http/osef-netbuffer/osef-posix/Socket -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/29dd86f/main.o ../../main.cpp

# Subprojects
.build-subprojects:
	cd ../../../NetBeans/OSEF_Yeelight && ${MAKE}  -f Makefile CONF=Release
	cd ../../../osef-ssdp/osef-http/osef-netbuffer/osef-posix/NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Release

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../../../NetBeans/OSEF_Yeelight && ${MAKE}  -f Makefile CONF=Release clean
	cd ../../../osef-ssdp/osef-http/osef-netbuffer/osef-posix/NetBeans/OSEF_POSIX && ${MAKE}  -f Makefile CONF=Release clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
