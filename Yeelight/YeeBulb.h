#ifndef OSEFYEEBULB_H
#define OSEFYEEBULB_H

#include "TcpClient.h"
#include "YeeFlow.h"

#include <string>

namespace OSEF
{
    const std::string YeelightProtocol = "yeelight";
    const std::string YeelightTcpPort = "55443";

    class YeeBulb
    {
    public:
        explicit YeeBulb(const std::string& h, const timespec& to = {1, 0});
        virtual ~YeeBulb();

        std::string getHost() const {return client.getHost();}

        bool getPower(bool& power);
        bool getBright(uint32_t& bright);

        bool on(const uint32_t& ms = 30);
        bool off(const uint32_t& ms = 30);

        bool temperature(const uint32_t& t, const uint32_t& ms = 30);
        bool color(const uint8_t& r, const uint8_t& g, const uint8_t&b, const uint32_t& ms = 30);
        bool hue(const uint16_t& hue, const uint8_t& sat, const uint32_t& ms = 30);
        bool brightness(const uint8_t& b, const uint32_t& ms = 30);

        bool startFlow(const YeeFlow& flow);

    private:
        YeeBulb(const YeeBulb& orig);
        YeeBulb& operator=(const YeeBulb& orig);

        bool sendCommand(const std::string& command);
        bool sendCommand(const std::string& command, std::string& returnValue);
        bool extractResponse(std::string& response, std::string& returnValue);

        void removeSeparationSpaces(std::string& response);
        bool removeEndings(std::string& s, const std::string& start, const std::string& end);
        bool popFront(std::string& s, const std::string& separator, std::string& extract);

        bool extractId(std::string& response);
        bool extractResult(std::string& response, std::string& value);
        bool extractValue(std::string& response, std::string& value);
        bool extractError(std::string& response, std::string& code, std::string& message);

        std::string getParameter(const std::string& s);
        std::string getParameter(const uint32_t& u);
        std::string getCommand(const std::string& method, const std::string& parameters);

        TcpClient client;
        uint32_t commandId;
        timespec responseTimeOut;
    };
}  // namespace OSEF

#endif /* OSEFYEEBULB_H */
