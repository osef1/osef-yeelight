#include "YeeController.h"
#include "Debug.h"

OSEF::YeeController::YeeController(const std::string& lh)
    :discoverer(lh, YeelightSsdpMulticastPort),
    bulbs(){}

OSEF::YeeController::~YeeController() {}

bool OSEF::YeeController::startFlow(const YeeFlow& flow)
{
    bool ret = true;

    for (BG_Element& bulb : bulbs)
    {
        ret &= bulb.startFlow(flow);
    }

    return ret;
}

bool OSEF::YeeController::on(const uint32_t& ms)
{
    bool ret = true;

    for (BG_Element& bulb : bulbs)
    {
        ret &= bulb.on(ms);
    }

    return ret;
}

bool OSEF::YeeController::off(const uint32_t& ms)
{
    bool ret = true;

    for (BG_Element& bulb : bulbs)
    {
        ret &= bulb.off(ms);
    }

    return ret;
}

bool OSEF::YeeController::getField(const std::string& bulbAddress, const std::string& field, std::string& value)
{
    const bool ret = discoverer.getField("yeelight://"+bulbAddress+":"+YeelightTcpPort, field, value);
    return ret;
}

bool OSEF::YeeController::getAddresses(BA_Container& addresses)
{
    bool ret = false;

    for (const YeeBulb& bulb : bulbs)
    {
        addresses.push_back(bulb.getHost());
        ret = true;
    }

    return ret;
}

bool OSEF::YeeController::search(const uint8_t& wait)
{
    bool ret = false;

    if (discoverer.search(wait, YeelightSsdpService, YeelightSsdpIdField))
    {
        OSEF::DI_Container identifications;
        if (discoverer.getIdentifications(identifications))
        {
            for (std::string& id : identifications)
            {
                SplitLocation location = {"", "", "", ""};
                if (discoverer.getSplitLocation(id, location))
                {
                    if ((location.protocol == YeelightProtocol) && (location.port == YeelightTcpPort) && (location.uri == ""))
                    {
                        bulbs.emplace_back(location.ip);
                        DOUT("Yeelight controller stored bulb " << location.ip);
                        ret = true;
                    }
                    else
                    {
                        DERR("error filtering discovered device");
                    }
                }
            }
        }
    }

    return ret;
}
