#include "YeeFlow.h"
#include "Debug.h"

OSEF::YeeFlow::YeeFlow() {}

OSEF::YeeFlow::~YeeFlow() {}

bool OSEF::YeeFlow::pushTemperatureStep(const uint32_t& temperature, const uint32_t& ms, const int32_t& bright)
{
    bool ret = false;

    if (ms >= 50)
    {
        if ( (bright == -1) || ((bright >= 1) && (bright <= 100)) )
        {
            if ((temperature >= 1700) && (temperature <= 6500))
            {
                flow.push_back({ms, 2, temperature, bright});
                ret = true;
            }
            else
            {
                DERR("temperature " << temperature << " out of range [1700,6500]");
            }
        }
        else
        {
            DERR("brightness " << bright << " out of range -1 or [1,100]");
        }
    }
    else
    {
        DERR("duration " << ms << " smaller than 50 ms");
    }

    return ret;
}

bool OSEF::YeeFlow::pushColorStep(const RgbUint8_t& rgb, const uint32_t& ms, const int32_t& bright)
{
    bool ret = false;

    if (ms >= 50)
    {
        if ( (bright == -1) || ((bright >= 1) && (bright <= 100)) )
        {
            flow.push_back({ms, 1, static_cast<uint32_t>((rgb.red << 16) + (rgb.green << 8) + rgb.blue), bright});
            ret = true;
        }
        else
        {
            DERR("brightness " << bright << " out of range -1 or [1,100]");
        }
    }
    else
    {
        DERR("duration " << ms << " smaller than 50 ms");
    }

    return ret;
}

bool OSEF::YeeFlow::pushSleep(const uint32_t& ms)
{
    bool ret = false;

    if (ms >= 50)
    {
        flow.push_back({ms, 7, 0, -1});
        ret = true;
    }
    else
    {
        DERR("duration " << ms << " smaller than 50 ms");
    }

    return ret;
}

std::string OSEF::YeeFlow::asString() const
{
    std::string ret = "";

    if (flow.size() > 0)
    {
        ret += "\"";

        for (const BF_Element& step : flow)
        {
//            ret += getStepString(step) + ",";
            ret.append(getStepString(step));
            ret.append(",");
        }

        ret.erase(ret.size()-1);

        ret += "\"";
    }

    return ret;
}

std::string OSEF::YeeFlow::getStepString(const BF_Element& s) const
{
    const std::string ret = std::to_string(s.duration)+","+std::to_string(s.mode)+","+std::to_string(s.value)+","+std::to_string(s.brightness);
    return ret;
}
