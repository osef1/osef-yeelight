#ifndef OSEFYEECONTROLLER_H
#define OSEFYEECONTROLLER_H

#include "SsdpClient.h"
#include "YeeBulb.h"

#include <string>
#include <list>

namespace OSEF
{
    const std::string YeelightSsdpMulticastPort = "1982";
    const std::string YeelightSsdpService = "wifi_bulb";
    const std::string YeelightSsdpIdField = SsdpLocationName;

    typedef YeeBulb BG_Element;
    typedef std::list<BG_Element> BG_Container;
    typedef BG_Container::iterator BG_Iterator;

    //  Bulbs Addresses
    typedef std::string BA_Element;
    typedef std::list<BA_Element> BA_Container;

    class YeeController
    {
    public:
        explicit YeeController(const std::string& lh);
        virtual ~YeeController();

        bool search(const uint8_t& wait = 1);

        bool getAddresses(BA_Container& addresses);

        bool getField(const std::string& bulbAddress, const std::string& field, std::string& value);

        bool on(const uint32_t& ms = 30);
        bool off(const uint32_t& ms = 30);
        bool startFlow(const YeeFlow& flow);

    private:
        YeeController(const YeeController& orig);
        YeeController& operator=(const YeeController& orig);

        SsdpClient discoverer;
        BG_Container bulbs;
    };
}  // namespace OSEF

#endif /* OSEFYEECONTROLLER_H */
