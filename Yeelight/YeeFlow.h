#ifndef OSEFYEEFLOW_H
#define OSEFYEEFLOW_H

#include <string>
#include <vector>

namespace OSEF
{
    typedef struct
    {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    }RgbUint8_t;

    typedef struct
    {
        uint32_t duration;  // Gradual change time or sleep time, in milliseconds, minimum value 50
        uint32_t mode;  // 1 – color, 2 – color temperature, 7 – sleep
        uint32_t value;  // RGB value when mode is 1, CT value when mode is 2, Ignored when mode is 7
        int32_t brightness;  // Brightness value, -1 or [1,100]. Ignored when -1 or mode is 7.
    }BF_Element;

    typedef std::vector<BF_Element> BF_Container;
//    typedef BF_Container::iterator BF_Iterator;

    class YeeFlow
    {
    public:
        YeeFlow();
        virtual ~YeeFlow();

        bool pushColorStep(const RgbUint8_t& rgb, const uint32_t& ms = 50, const int32_t& bright = -1);
        bool pushTemperatureStep(const uint32_t& temperature, const uint32_t& ms = 50, const int32_t& bright = -1);
        bool pushSleep(const uint32_t& ms = 50);

        size_t getSize() const {return flow.size();}
        std::string asString() const;

    private:
        YeeFlow(const YeeFlow& orig);
        YeeFlow& operator=(const YeeFlow& orig);

        std::string getStepString(const BF_Element& t) const;

        BF_Container flow;
    };
}  // namespace OSEF

#endif /* OSEFYEEFLOW_H */
