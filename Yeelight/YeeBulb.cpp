#include "YeeBulb.h"
#include "Debug.h"

OSEF::YeeBulb::YeeBulb(const std::string& h, const timespec& to)
    :client(h, YeelightTcpPort),
    commandId(0),
    responseTimeOut(to) {}

OSEF::YeeBulb::~YeeBulb() {}

// {"id":1,"method":"get_prop","params":["power", "not_exist", "bright"]}
bool OSEF::YeeBulb::getPower(bool& power)
{
    bool ret = false;

    std::string pw = "";
    if (sendCommand(getCommand("get_prop", getParameter("power")), pw))
    {
        if (pw == "on")
        {
            power = true;
            ret = true;
        }
        else if (pw == "off")
        {
            power = false;
            ret = true;
        }
    }

    return ret;
}

bool OSEF::YeeBulb::getBright(uint32_t& bright)
{
    bool ret = false;

    std::string b = "";
    if (sendCommand(getCommand("get_prop", getParameter("bright")), b))
    {
        uint64_t tmp = std::stoul(b);
        if (tmp <= 100UL)
        {
            bright = tmp;
            ret = true;
        }
    }

    return ret;
}

// {"id":1,"method":"set_power","params":["on", "smooth", 30]}
bool OSEF::YeeBulb::on(const uint32_t& ms)
{
    const bool ret = sendCommand(getCommand("set_power", getParameter("on")+","+getParameter("smooth")+","+getParameter(ms)));
    return ret;
}

bool OSEF::YeeBulb::off(const uint32_t& ms)
{
    const bool ret = sendCommand(getCommand("set_power", getParameter("off")+",\"smooth\","+getParameter(ms)));
    return ret;
}

bool OSEF::YeeBulb::sendCommand(const std::string& command)
{
    std::string value = "";
    const bool ret = sendCommand(command, value);
    return ret;
}

bool OSEF::YeeBulb::sendCommand(const std::string& command, std::string& returnValue)
{
    bool ret = false;

//    _DOUT("command="<<command);
    if (client.sendString(command))
    {
        std::string response = "";
        if (client.receiveString(response, 1024, responseTimeOut))
        {
            ret = extractResponse(response, returnValue);
        }
        else
        {
            DWARN("Bulb " << client.getHost() << " did not respond to command " << command);
        }
    }

    return ret;
}

// {"id":0,"result":["ok"]}
// {"id":2,"error":{"code":-1,“message”:"unsupported method"}}
// {"id":3,"result":["on"]}
bool OSEF::YeeBulb::extractResponse(std::string& response, std::string& returnValue)
{
    bool ret = false;

//    _DOUT("response="<<response);

    if (extractId(response))
    {
        ret = extractResult(response, returnValue);
    }

    return ret;
}

void OSEF::YeeBulb::removeSeparationSpaces(std::string& response)
{
//    _DOUT("response with spaces        " << response);

    size_t space = response.find(", ");
    while (space != std::string::npos)
    {
        response.erase(space + 1, 1);
        space = response.find(", ");
    }

//    _DOUT("response without spaces     " << response);
}

bool OSEF::YeeBulb::removeEndings(std::string& s, const std::string& start, const std::string& end)
{
    bool ret = false;

//    _DOUT("with endings " << s);

    if (s.find(start) == 0)
    {
        s.erase(0, start.size());

        if (s.find(end) == (s.size() - end.size()))
        {
            s.erase(s.size() - end.size(), end.size());
            ret = true;
        }
    }

//    _DOUT("without endings " << s);

    return ret;
}

bool OSEF::YeeBulb::popFront(std::string& s, const std::string& separator, std::string& pop)
{
    bool ret = false;

//    _DOUT("before front pop " << s);

    size_t pos = s.find(separator);
    if (pos != std::string::npos)
    {
        pop = s.substr(0, pos);
//        _DOUT("pop="<<pop);

        s.erase(0, pos + separator.size());
        ret = true;
    }

//    _DOUT("after front pop " << s);

    return ret;
}

bool OSEF::YeeBulb::extractId(std::string& response)
{
    bool ret = false;

    removeSeparationSpaces(response);

    if (removeEndings(response, "{\"id\":", "}\r\n"))
    {
        std::string id = "";
        if (popFront(response, ",\"", id))
        {
            if (id == std::to_string(commandId-1))
            {
                ret = true;
            }
        }
    }

    return ret;
}

bool OSEF::YeeBulb::extractResult(std::string& response, std::string& value)
{
    bool ret = false;

    std::string result = "";
    if (popFront(response, "\":", result))
    {
        if (result == "result")
        {
            ret = extractValue(response, value);
        }
        else
        {
            std::string code = "";
            std::string message = "";
            if (extractError(response, code, message))
            {
                DWARN("bulb returned error " << code << " " << message);
            }
        }
    }

    return ret;
}

bool OSEF::YeeBulb::extractValue(std::string& response, std::string& value)
{
    bool ret = false;

    if (removeEndings(response, "[\"", "\"]"))
    {
        value = response;
        ret = true;
    }

    return ret;
}

// {"code":-1,"message":"method not supported"}
bool OSEF::YeeBulb::extractError(std::string& response, std::string& code, std::string& message)
{
    bool ret = false;

    if (removeEndings(response, "{\"code\":", "\"}"))
    {
        if (popFront(response, ",\"message\":\"", code))
        {
            message = response;
            ret = true;
        }
    }

    return ret;
}
// Kelvin between 1700 and 6500
// {"id":1,"method":"set_ct_abx","params":[6500, "smooth", 30]}
bool OSEF::YeeBulb::temperature(const uint32_t& t, const uint32_t& ms)
{
    bool ret = false;

    if ((t >= 1700) && (t <= 6500))
    {
        if (ms >= 30)
        {
            ret = sendCommand(getCommand("set_ct_abx", getParameter(t)+",\"smooth\","+getParameter(ms)));
        }
        else
        {
            DERR("duration " << ms << " smaller than 30 ms");
        }
    }
    else
    {
        DERR("temperature " << t << " out of range [1700,6500]");
    }

    return ret;
}

// {"id":1,"method":"set_rgb","params":[255, "smooth", 30]}
bool OSEF::YeeBulb::color(const uint8_t& r, const uint8_t& g, const uint8_t&b, const uint32_t& ms)
{
    bool ret = false;

    if (ms >= 30)
    {
        ret = sendCommand(getCommand("set_rgb", getParameter((r << 16)+(g << 8)+b)+",\"smooth\","+getParameter(ms)));
    }
    else
    {
        DERR("duration " << ms << " smaller than 30 ms");
    }

    return ret;
}

// {"id":1,"method":"set_hsv","params":[255, 45, "smooth", 500]}
bool OSEF::YeeBulb::hue(const uint16_t& hue, const uint8_t& sat, const uint32_t& ms)
{
    bool ret = false;

    if (hue <= 359)
    {
        if (sat <= 100)
        {
            if (ms >= 30)
            {
                ret = sendCommand(getCommand("set_hsv", getParameter(hue)+","+getParameter(sat)+",\"smooth\","+getParameter(ms)));
            }
            else
            {
                DERR("duration " << ms << " smaller than 30 ms");
            }
        }
        else
        {
            DERR("saturation " << sat << " greater than 100");
        }
    }
    else
    {
        DERR("hue " << hue << " greater than 359");
    }

    return ret;
}

// set_bright
// Usage:
// This method is used to change the brightness of a smart LED.Parameters:
// 3."brightness"is the target brightness. The type is integer and ranges from 1to 100.
// The brightness is a percentage instead of a absolute value. 100means maximum brightness while 1means the minimum brightness.
//         "effect": Refer to "set_ct_abx" method."duration": Refer to "set_ct_abx" method.
//         Request Example:     {"id":1,"method":"set_bright","params":[50, "smooth", 500]}
bool OSEF::YeeBulb::brightness(const uint8_t& b, const uint32_t& ms)
{
    bool ret = false;

    if ((b >= 1) && (b <= 100))
    {
        if (ms >= 30)
        {
            ret = sendCommand(getCommand("set_bright", getParameter(b)+",\"smooth\","+getParameter(ms)));
        }
        else
        {
            DERR("duration " << ms << " smaller than 30 ms");
        }
    }
    else
    {
        DERR("brightness " << b << " out of range [1,100]");
    }

    return ret;
}

bool OSEF::YeeBulb::startFlow(const YeeFlow& flow)
{
    const bool ret =  sendCommand(getCommand("start_cf", getParameter(flow.getSize())+",0,"+flow.asString()));
    return ret;
}

std::string OSEF::YeeBulb::getParameter(const std::string& s)
{
    std::string param = '"'+s+'"';
    return param;
}

std::string OSEF::YeeBulb::getParameter(const uint32_t& u)
{
    std::string param = std::to_string(u);
    return param;
}

std::string OSEF::YeeBulb::getCommand(const std::string& method, const std::string& parameters)
{
    // {"id":"commandId","method":"method","params":["parameters"]}\r\n
    std::string command = "{\"id\":"+std::to_string(commandId)+",\"method\":\""+method+"\",\"params\":["+parameters+"]}\r\n";
    commandId++;
//    _DOUT("command " << command.substr(0, command.size()-2));
    return command;
}
