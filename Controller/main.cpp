#include "YeeController.h"

#include <getopt.h>
#include <iostream>

 bool getInterfaceName(int argc, char** argv, std::string& ifName)
{
    bool ret = true;
    int32_t opt = -1;

    if (argc > 1)
    {
        do
        {
            opt = getopt(argc, argv, "i:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'i':
                        ifName.assign(optarg);
                        break;
                    default:
                        std::cout << "Usage:   " << argv[0] << " [-option] [argument]" << std::endl;
                        std::cout << "option:  " << std::endl;
                        std::cout << "         " << "-i  Ethernet interface (wlan0)" << std::endl;
                        ret = false;
                        break;
                }
            }
        }while (ret && (opt != -1));
    }

    return ret;
}

 int displayAddresses(OSEF::YeeController& controller)
 {
    int ret = 0;

    OSEF::BA_Container addresses;
    if (controller.getAddresses(addresses))
    {
        ret = addresses.size();

        for (OSEF::BA_Element address : addresses)
        {
            std::string id = "unknownId";
            if (not controller.getField(address, "ID", id))
            {
                ret = false;
            }

            std::string name = "unknownName";
            if (not controller.getField(address, "NAME", name))
            {
                ret = false;
            }

            std::string model = "unknownModel";
            if (not controller.getField(address, "MODEL", model))
            {
                ret = false;
            }

            std::string version = "unknownVersion";
            if (not controller.getField(address, "FW_VER", version))
            {
                ret = false;
            }

            std::cout << "\"" << name << "\" " << id << " " << model << " (v" << version << ") bulb at " << address << std::endl;
        }
    }

    return ret;
 }

int main(int argc, char** argv)
{
    int ret = -1;

    std::string interface = "wlan0";
    if (getInterfaceName(argc, argv, interface))
    {
        OSEF::YeeController controller(interface);
        if (controller.search())
        {
            if (displayAddresses(controller) > 0)
            {
                ret = 0;
            }
        }
    }

    return ret;
}
