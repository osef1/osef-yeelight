#!/bin/bash

# add osef-ref submodule
git submodule add https://gitlab.com/osef1/osef-ref.git

# update osef-ref scripts
cd osef-ref
./update.sh ..
cd ..

# remove osef-ref submodule
git submodule deinit --quiet -f osef-ref
git rm --quiet -f osef-ref
rm -rf .git/modules/osef-ref
